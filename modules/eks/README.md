# Terraform EKS Module

## Table of Contents

- [Terraform EKS Module](#terraform-eks-module)
  - [Why Modular?](#why-modular)
- [Understanding oidc.tf and main.tf Configurations](#understanding-oidctf-and-maintf-configurations)
  - [oidc.tf](#oidctf)
  - [main.tf](#maintf)
- [Usage](#usage)
  - [Prerequisites](#prerequisites)
  - [Steps to use this module](#steps-to-use-this-module)
  - [Variables available for configuring the Cluster via this module](#variables-available-for-configuring-the-cluster-via-this-module)
- [Architecture Diagram](#architecture-diagram)


This Terraform module provides a set of configurations for deploying an Amazon EKS (Elastic Kubernetes Service) cluster in a modular and scalable fashion. The module is designed to be flexible and easy to use, allowing you to deploy a secure and production-ready EKS cluster with minimal setup.

## Why Modular?

The module is divided into several files, each serving a specific purpose, ensuring that each aspect of the EKS deployment is handled in isolation, promoting better code management and reusability.

- `main.tf`: Contains the core logic for deploying an EKS cluster.
- `oidc.tf`: Manages OIDC provider configuration, enabling IAM roles for service accounts (IRSA).
- `outputs.tf`: Defines the outputs that the module will expose, allowing easy integration with other configurations or modules.
- `variables.tf`: Contains variable declarations, making the module dynamic and customizable.
- `terraform.tf`: Specifies the required providers and potentially their versions.

### Understanding `oidc.tf` and `main.tf` Configurations

In this section, we delve into the specifics of the oidc.tf and main.tf files within the EKS Terraform module, providing insights into their functionalities and configurations.

#### `oidc.tf`

The oidc.tf file within the EKS Terraform module is dedicated to managing the OpenID Connect (OIDC) identity provider configuration for the EKS cluster. OIDC is an identity layer on top of the OAuth 2.0 protocol, which allows AWS services to communicate with Kubernetes API Server. This file is particularly crucial for setting up IAM Roles for Service Accounts (IRSA) which allows you to associate IAM roles with Kubernetes service accounts.

**Key Components and Configuration:**

1. **OIDC Provider Data Retrieval**

- The tls_certificate data block fetches the OIDC issuer URL's certificate, which is necessary to establish trust between the EKS cluster and AWS IAM.
- The count attribute conditionally fetches the certificate only when enable_iam_roles_for_service_account is set to true.

```hcl
data "tls_certificate" "cluster_oidc_endpoint" {
  count = var.enable_iam_roles_for_service_account ? 1 : 0
  url   = aws_eks_cluster.this.identity[0].oidc[0].issuer
}
```

2. **OIDC Provider Creation**

- The aws_iam_openid_connect_provider resource block creates an OIDC identity provider in IAM.
- The client_id_list is set to ["sts.amazonaws.com"] to allow the Security Token Service to assume roles to provide temporary tokens.
- The thumbprint_list is set to the SHA-1 fingerprint of the certificate fetched by the tls_certificate data block.
- The url is set to the OIDC issuer URL associated with the EKS cluster.
- The count attribute conditionally creates the OIDC provider only when enable_iam_roles_for_service_account is set to true.

```hcl
resource "aws_iam_openid_connect_provider" "cluster_oidc_provider" {
  count           = var.enable_iam_roles_for_service_account ? 1 : 0
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = data.tls_certificate.cluster_oidc_endpoint[0].certificates[*].sha1_fingerprint
  url             = data.tls_certificate.cluster_oidc_endpoint[0].url
}
```

Usage in IAM Role for Service Account:

The OIDC provider is used to establish trust between the EKS cluster and AWS IAM, allowing Kubernetes service accounts to assume IAM roles. This is particularly useful for granting AWS permissions to Kubernetes pods.

Example IAM role trust policy utilizing the OIDC provider:

```json
{
  "Effect": "Allow",
  "Principal": {"Federated": "arn:aws:iam::[ACCOUNT_ID]:oidc-provider/[OIDC_PROVIDER_URL]"},
  "Action": "sts:AssumeRoleWithWebIdentity",
  "Condition": {
    "StringEquals": {
      "[OIDC_PROVIDER_URL]:sub": "system:serviceaccount:[NAMESPACE]:[SERVICE_ACCOUNT_NAME]"
    }
  }
}
```

Why `oidc.tf` is Important:

- **Security**: Enables Kubernetes workloads to securely access AWS services without storing AWS credentials in Kubernetes secrets.
- **Granular Permissions**: Allows you to define fine-grained permissions and associate them with Kubernetes service accounts.
- **Scalability**: Ensures that as your workloads scale, they can securely interact with AWS services using IAM roles.

#### `main.tf`

The main.tf file serves as the core of the EKS Terraform module, containing the primary configurations and resources required to create an EKS cluster. This file defines the EKS cluster, its associated settings, and manages its lifecycle.

**Key Components and Configuration:**

1. **Local Values**

- `public_access_cidrs`: Conditionally set based on whether public access is enabled.
- `enabled_cluster_log_types`: Determines which control plane logging configurations to enable.

```hcl
locals {
  public_access_cidrs       = var.enable_public_access ? var.public_access_cidrs : []
  enabled_cluster_log_types = [for k, v in tomap(var.logging_config) : k if v]
}
```

2. **EKS Cluster Resource**

- `aws_eks_cluster`: The resource block that provisions the EKS cluster.
- `name`, `version`, and role_arn are set using variables.
- `vpc_config` block configures VPC-related settings, such as subnet IDs, security group IDs, and endpoint access.
- `enabled_cluster_log_types` is set using the local value.
- `encryption_config` block conditionally sets up envelope encryption for Kubernetes secrets using KMS.

```hcl
resource "aws_eks_cluster" "this" {
  depends_on = [aws_cloudwatch_log_group.cluster_log_group]
  name       = var.cluster_name
  version    = var.kubernetes_version
  role_arn   = var.cluster_role
  vpc_config {
    subnet_ids              = var.control_plane_subnets
    security_group_ids      = var.control_plane_security_groups
    endpoint_private_access = true
    endpoint_public_access  = var.enable_public_access
    public_access_cidrs     = local.public_access_cidrs
  }
  enabled_cluster_log_types = local.enabled_cluster_log_types

  dynamic "encryption_config" {
    for_each = var.encryption_kms_key_arn == "" ? [] : [var.encryption_kms_key_arn]
    content {
      provider {
        key_arn = encryption_config.value
      }
      resources = ["secrets"]
    }
  }
}
```

3. **EKS Cluster Authentication Data**

- `aws_eks_cluster_auth`: The data block that retrieves authentication credentials for the EKS cluster.
- `name` is set using the variable.

```hcl
data "aws_eks_cluster_auth" "this" {
  depends_on = [aws_eks_cluster.this]
  name       = var.cluster_name
}
```

Why `main.tf` is Important:
- **Cluster Provisioning**: Defines and provisions the EKS cluster based on the provided variables.
- **VPC Configuration**: Manages the VPC configurations, ensuring the EKS control plane is associated with the correct network resources.
- **Security**: Manages the security settings of the EKS cluster, including IAM roles and KMS encryption.
- **Logging**: Configures logging for various EKS control plane components, enhancing observability.
- **Scalability & Flexibility**: Ensures that the module can be easily scaled and customized by adjusting the input variables.


## Usage

### Prerequisites

- Install [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) and [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).
- Configure AWS CLI with the necessary credentials.

### Steps to use this module

1. Create a `main.tf` file in your project directory and utilize the EKS module:

```hcl
module "my_eks_cluster" {
  source = "path_to_your_module/eks"

  cluster_name    = "my-eks-cluster"
  cluster_role    = "arn:aws:iam::123456789012:role/eks-cluster-role"
  kubernetes_version = "1.21"

  control_plane_subnets = [
    "subnet-0bb1c79de3EXAMPLE",
    "subnet-0bb1c79de3EXAMPLE",
    "subnet-0bb1c79de3EXAMPLE"
  ]

  control_plane_security_groups = [
    "sg-0123456789EXAMPLE"
  ]

  enable_public_access = true
  public_access_cidrs  = ["0.0.0.0/0"]

  enable_iam_roles_for_service_account = true

  logging_config = {
    api               = true
    audit             = true
    authenticator     = true
    controllerManager = false
    scheduler         = false
  }

  encryption_kms_key_arn = "arn:aws:kms:us-west-2:123456789012:key/abcd1234-a123-456a-a12b-a123b4cd56ef"
}
```

2. Initialize and Apply

```sh
terraform init
terraform apply
```

3. Access and manage the EKS Cluster by configuring `kubectl` to use the new EKS cluster configs.

```sh
aws eks update-kubeconfig --region [region] --name my-eks-cluster
kubectl get nodes
```

### Variables available for configuring the Cluster via this module

| Variable Name                         | Type           | Default Value | Description                                                                                                           |
|---------------------------------------|----------------|---------------|-----------------------------------------------------------------------------------------------------------------------|
| `cluster_name`                        | string         | N/A           | The name of the EKS cluster.                                                                                          |
| `control_plane_subnets`               | list(string)   | N/A           | A list of subnet IDs to associate with the EKS control plane.                                                         |
| `control_plane_security_groups`       | list(string)   | `[]`          | A list of security group IDs to associate with the EKS control plane.                                                 |
| `cluster_role`                        | string         | N/A           | The Amazon Resource Name (ARN) of the IAM role that provides permissions for the EKS cluster.                         |
| `kubernetes_version`                  | string         | `"1.27"`      | The desired Kubernetes version for the EKS cluster. Ensure this version is valid.                                     |
| `enable_public_access`                | bool           | `false`       | Whether to enable public access to the EKS cluster API server.                                                        |
| `public_access_cidrs`                 | list(string)   | `[]`          | A list of CIDR blocks allowed to access the EKS cluster API server when public access is enabled.                     |
| `enable_iam_roles_for_service_account`| bool           | `true`        | Whether to enable IAM roles for Kubernetes service accounts (IRSA) in the EKS cluster.                                |
| `log_group_retention_period`          | number         | `0`           | The number of days to retain log events in the log group. Set to '0' to retain logs indefinitely.                     |
| `logging_config`                      | object         | See below     | A map defining the logging configuration for various EKS control plane components.                                    |
| `encryption_kms_key_arn`              | string         | `""`          | The Amazon Resource Name (ARN) of the KMS key used for envelope encryption of secrets in the EKS cluster.              |

For `logging_config`, the default object is:

```hcl
{
  api               = true
  audit             = true
  authenticator     = true
  controllerManager = false
  scheduler         = false
}
```

- `api`: Enables or disables logging for the Kubernetes API server component.
- `audit`: Enables or disables audit logging.
- `authenticator`: Enables or disables logging for the authenticator component.
- `controllerManager`: Enables or disables logging for the controller manager component.
- `scheduler`: Enables or disables logging for the scheduler component.

## Architecture Diagram

Below is a diagram illustrating the architecture of the EKS setup using this Terraform module:

```mermaid
graph TB
    subgraph "Terraform Module"
        oidc[oidc.tf] -->|Provisions| oidcProvider(AWS OIDC Provider)
        main[main.tf] -->|Provisions| eks(EKS Cluster)
    end
    subgraph "AWS"
        oidcProvider -->|Trusts| iamRole(AWS IAM Role)
        eks -->|Associates with| oidcProvider
        eks -->|Uses| iamRole
    end
    subgraph "Kubernetes"
        sa(K8s Service Account) -->|Assumes| iamRole
        pod(Pod) -->|Uses| sa
    end
    iamRole -->|Grants permissions to| awsServices{AWS Services}
    pod -->|Interacts with| awsServices
    style oidc fill:#ffcccc,stroke:#333,stroke-width:2px
    style main fill:#ccffcc,stroke:#333,stroke-width:2px
    style oidcProvider fill:#ccccff,stroke:#333,stroke-width:2px
    style eks fill:#ccccff,stroke:#333,stroke-width:2px
    style iamRole fill:#ccccff,stroke:#333,stroke-width:2px
    style sa fill:#ccccff,stroke:#333,stroke-width:2px
    style pod fill:#ccccff,stroke:#333,stroke-width:2px
    style awsServices fill:#ccccff,stroke:#333,stroke-width:2px
```

