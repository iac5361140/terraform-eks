variable "cluster_name" {
  type        = string
  description = "The name of the EKS cluster."
}

variable "control_plane_subnets" {
  type        = list(string)
  description = "A list of subnet IDs to associate with the EKS control plane."
}

variable "control_plane_security_groups" {
  type        = list(string)
  default     = []
  description = "A list of security group IDs to associate with the EKS control plane."
}

variable "cluster_role" {
  type        = string
  description = "The Amazon Resource Name (ARN) of the IAM role that provides permissions for the EKS cluster."
}

variable "kubernetes_version" {
  type        = string
  default     = "1.27"
  description = "The desired Kubernetes version for the EKS cluster."
}

variable "enable_public_access" {
  type        = bool
  default     = false
  description = "Whether to enable public access to the EKS cluster API server."
}

variable "public_access_cidrs" {
  type        = list(string)
  default     = []
  description = "A list of CIDR blocks allowed to access the EKS cluster API server when public access is enabled."
}

variable "enable_iam_roles_for_service_account" {
  type        = bool
  default     = true
  description = "Whether to enable IAM roles for Kubernetes service accounts (IRSA) in the EKS cluster."
}

variable "log_group_retention_period" {
  type        = number
  default     = 0
  description = "The number of days to retain log events in the log group. Set to '0' to retain logs indefinitely."
}

variable "logging_config" {
  type = object({
    api               = bool
    audit             = bool
    authenticator     = bool
    controllerManager = bool
    scheduler         = bool
  })
  default     = {
    api               = true
    audit             = true
    authenticator     = true
    controllerManager = false
    scheduler         = false
  }
  description = "A map defining the logging configuration for various EKS control plane components."
}

variable "encryption_kms_key_arn" {
  type        = string
  default     = ""
  description = "The Amazon Resource Name (ARN) of the KMS key used for envelope encryption of secrets in the EKS cluster. Leave empty to disable."
}
