variable "cluster_name" {
  type = string
  description = "The name designated for the EKS cluster. This name is utilized to tag resources and construct unique resource names to avoid conflicts."
}

variable "vpc_id" {
  type = string
  description = "The ID of the VPC where the EKS cluster and associated resources will be deployed."
}

variable "control_plane_subnet_ids" {
  type    = set(string)
  default = []
  description = "A set of subnet IDs for deploying the EKS control plane. These subnets should be part of the provided VPC and are typically private subnets."
}

variable "control_plane_subnet_name_selectors" {
  type    = set(string)
  default = []
  description = "A set of subnet name tags to select additional subnets for deploying the EKS control plane. Subnets with matching name tags in the provided VPC will be utilized."
}

variable "node_group_subnet_ids" {
  type    = set(string)
  default = []
  description = "A set of subnet IDs for deploying the EKS node groups. These subnets should be part of the provided VPC."
}

variable "node_group_subnet_name_selectors" {
  type    = set(string)
  default = []
  description = "A set of subnet name tags to select additional subnets for deploying the EKS node groups. Subnets with matching name tags in the provided VPC will be utilized."
}

variable "whitelist_public_cidrs" {
  type    = set(string)
  default = []
  description = "A set of CIDR blocks that will be whitelisted to access the EKS cluster’s public API endpoint. Only relevant if public access is enabled."
}

variable "whitelist_caller_public_ip" {
  type    = bool
  default = true
  description = "If set to true, the IP address of the caller (user/CI/CD IP) will be automatically whitelisted to access the EKS cluster’s public API endpoint."
}

variable "enable_serviceroles" {
  type    = bool
  default = true
  description = "If set to true, IAM roles for Kubernetes service accounts (IRSA) will be enabled, allowing Kubernetes workloads to assume AWS IAM roles."
}

variable "logging_config" {
  type = object({
    api               = bool
    audit             = bool
    authenticator     = bool
    controllerManager = bool
    scheduler         = bool
  })
  default = {
    api               = true
    audit             = true
    authenticator     = true
    controllerManager = false
    scheduler         = false
  }
  description = "A map defining whether various EKS control plane logging components are enabled. Includes API server, audit, authenticator, controller manager, and scheduler logs."
}

variable "log_group_retention_period" {
  type    = number
  default = 7
  description = "The number of days to retain log events in the CloudWatch log group. Set to '0' to retain logs indefinitely."
}

variable "enable_secret_encryption" {
  type    = bool
  default = true
  description = "If set to true, Kubernetes secrets will be encrypted using AWS KMS."
}

variable "secret_encryption_kms_key_arn" {
  type    = string
  default = ""
  description = "The ARN of the AWS KMS key used to encrypt Kubernetes secrets. If empty and secret encryption is enabled, a new KMS key will be created."
}

variable "control_plane_security_groups" {
  type    = list(string)
  default = []
  description = "A list of security group IDs to associate with the EKS control plane. These security groups should allow necessary traffic to and from the EKS API server."
}

variable "kubernetes_version" {
  type    = string
  default = "1.27"
  description = "The desired Kubernetes version for the EKS cluster. Ensure this version is supported by AWS EKS."
}

variable "node_groups" {
  type = list(object({
    name                       = string
    desired_instance_count     = number
    max_instance_count         = number
    min_instance_count         = number
    capacity_type              = optional(string, "ON_DEMAND")
    disk_size                  = optional(number, 30)
    instance_type              = optional(string, "t2.medium")
    os                         = optional(string, "amazon-linux-2")
    max_unavailable_percentage = optional(number, 50)
  }))
  description = "A list of objects, each defining a node group within the EKS cluster. Each object allows customization of the node group’s instance type, scaling settings, and more."
}

variable "secret_encryption_key_deletion_window" {
  type    = number
  default = 7
  description = "The number of days to retain the KMS key used for secret encryption after it is scheduled for deletion. Only relevant if a KMS key is created by the module."
}
