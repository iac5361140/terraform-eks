# Terraform Cluster Module

## Table of Contents

- [Overview](#overview)
- [Understanding Configurations](#understanding-configurations)
    - [`control_plane.tf`](#control_planetf)
    - [`encryption.tf`](#encryptiontf)
    - [`network.tf`](#networktf)
    - [`node_group.tf`](#node_grouptf)
- [Usage](#usage)
    - [Prerequisites](#prerequisites)
    - [Required Modules](#required-modules)
    - [Steps to use this module](#steps-to-use-this-module)
- [Variables available for configuring the Cluster via this module](#variables-available-for-configuring-the-cluster-via-this-module)
- [Architecture Diagram](#architecture-diagram)

## Overview

This Terraform module provides configurations for creating a scalable and flexible Amazon EKS (Elastic Kubernetes Service) cluster, utilizing best practices for security and operability. The module is divided into several files, each serving a specific purpose, ensuring that each aspect of the EKS deployment is handled in isolation, promoting better code management and reusability.

## Understanding `control_plane.tf`, `encryption.tf`, `network.tf`, and `node_group.tf` Configurations

### `control_plane.tf`

**Key Components and Configuration**

1. **EKS Module Invocation**
- The module "eks" block invokes the EKS module to create and manage the EKS control plane.
- Various configurations like `cluster_name`, `control_plane_subnets`, and `logging_config` are passed as variables to tailor the EKS control plane according to specific needs.

```hcl
module "eks" {
  source                               = "../eks"
  cluster_name                         = var.cluster_name
  control_plane_subnets                = local.control_plane_subnet_ids
  control_plane_security_groups        = var.control_plane_security_groups
  logging_config                       = var.logging_config
  log_group_retention_period           = var.log_group_retention_period
  enable_iam_roles_for_service_account = var.enable_serviceroles
  encryption_kms_key_arn               = local.create_kms_key ? aws_kms_alias.secret_encryption_key_alias[0].arn : var.secret_encryption_kms_key_arn
  cluster_role                         = module.iam_roles.cluster_role_arn
  kubernetes_version                   = var.kubernetes_version
  enable_public_access                 = length(local.whitelist_public_cidrs) > 0
  public_access_cidrs                  = local.whitelist_public_cidrs
}
```

Why `control_plane.tf` is Important:

- **Cluster Management**: Manages the EKS control plane, which is crucial for managing worker nodes and handling cluster operations.
- **Security**: Ensures that the control plane is securely configured, with appropriate logging and optional encryption for secrets.
- **Network Configuration**: Associates the control plane with specified subnets and security groups, ensuring network isolation and security.

### `encryption.tf`

**Key Components and Configuration**

1. **KMS Key Creation**

- The `aws_kms_key` resource block conditionally creates a KMS key for secret encryption based on the create_kms_key local value.
- The `description` and deletion_window_in_days attributes are configured using variables.

```hcl
resource "aws_kms_key" "secret_encryption_key" {
  count                   = local.create_kms_key ? 1 : 0
  description             = "Key used to ecrypt secrets on ${var.cluster_name} EKS cluster"
  deletion_window_in_days = var.secret_encryption_key_deletion_window
}
```

2. **KMS Key Alias Creation**

- The `aws_kms_alias` resource block creates an alias for the KMS key, making it easier to reference and manage.

```hcl
resource "aws_kms_alias" "secret_encryption_key_alias" {
  count         = local.create_kms_key ? 1 : 0
  target_key_id = aws_kms_key.secret_encryption_key[0].id
  name          = "alias/eks/${var.cluster_name}"
}
```

Why `encryption.tf` is Important:

- **Secret Management**: Ensures that Kubernetes secrets are encrypted using KMS, enhancing the security of sensitive information.
- **Operational Ease**: Utilizing a KMS alias provides a human-readable reference to the KMS key, simplifying management and usage.

### `network.tf`

**Key Components and Configuration**

1. **Subnet Data Retrieval**

- The `data "aws_subnets"` blocks retrieve subnet IDs based on provided selectors or direct IDs, ensuring the EKS control plane and node groups are associated with the correct subnets.

```hcl
data "aws_subnets" "control_plane_subnets" {
  for_each = var.control_plane_subnet_name_selectors
  filter {
    name = "vpc-id"
    values = [
      var.vpc_id
    ]
  }
  tags = {
    "Name" = each.value
  }
}
```

2. **Caller IP Whitelisting**

- The `module "caller_details"` block conditionally retrieves the public IP of the caller, which can be whitelisted for secure access to the EKS API server.

```hcl
module "caller_details" {
  count  = var.whitelist_caller_public_ip ? 1 : 0
  source = "../helpers/get_local_details"
}
```

Why `network.tf` is Important:

- **Network Isolation**: Ensures that the EKS control plane and worker nodes are associated with the correct subnets, maintaining network isolation and security.
- **Secure Access**: Optionally whitelisting the caller’s IP ensures that only known IPs can access the EKS API server if public access is enabled.

### `node_group.tf`

**Key Components and Configuration**

1. **Node Group Configuration**

- The `locals` block constructs node group configurations by combining provided variables and defaults, ensuring that node groups are configured according to specific needs.

```hcl
locals {
  node_groups = [
    for ng_ in var.node_groups : {
      name           = "${var.cluster_name}-${ng_.name}"
      instance_types = [ng_.instance_type]
      role_arn       = module.iam_roles.node_group_role_arn
      capacity_type  = ng_.capacity_type
      subnet_ids     = local.node_group_subnet_ids
      disk_size      = ng_.disk_size
      scaling_config = {
        desired_size = ng_.desired_instance_count
        min_size     = ng_.min_instance_count
        max_size     = ng_.max_instance_count
      }
      max_unavailable_percentage = ng_.max_unavailable_percentage
    }
  ]
}
```

2. **Node Group Module Invocation**

- The `module "node_group"` block invokes the `node_group` module to create and manage EKS node groups based on the constructed configurations.

```hcl
module "node_group" {
  depends_on         = [module.iam_roles, module.eks]
  source             = "../node_group"
  cluster_name       = var.cluster_name
  node_groups        = local.node_groups
  kubernetes_version = var.kubernetes_version
}
```

Why `node_group.tf` is Important:

- **Worker Nodes:** Ensures that worker nodes, which run the containers, are provisioned, managed, and configured appropriately.
- **Scalability:** Enables the EKS cluster to scale by managing multiple node groups with varying configurations.
- **High Availability:** Ensures that workloads can be distributed across multiple availability zones, enhancing the availability and reliability of applications.

## Usage

### Prerequisites

- Install [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) and [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).
- Configure AWS CLI with the necessary credentials.

### Required Modules
Ensure the following modules are available as they are invoked in the configurations:

- **EKS Module**: Ensure the EKS module is available at the path `../eks` as it is invoked in `control_plane.tf`.
- **IAM Roles Module**: Ensure the IAM roles module is available at the path `../iam_roles` as it is invoked in `iam.tf`.
- **Node Group Module**: Ensure the node group module is available at the path `../node_group` as it is invoked in `node_group.tf`.
- **Caller Details Module** (optional): Ensure the caller details module is available at the path `../helpers/get_local_details` as it is conditionally invoked in `network.tf`.

### Steps to use this module

1. Create a `main.tf` file in your project directory and utilize the module by declaring it and providing the necessary variables. Ensure to specify the source path to the module.

```hcl
module "eks_cluster" {
  source = "path_to_your_module/cluster"

  cluster_name = "my-eks-cluster"
  vpc_id       = "vpc-0abcd1234efgh5678"
  # ... other variables
}
```

Replace `"path_to_your_module/cluster"` with the actual path to the module and provide appropriate values for the variables.

2. Define and configure the variables required by the module in a `variables.tf` or `terraform.tfvars` file. Ensure to provide values for all required variables.

Example terraform.tfvars:
```hcl
cluster_name = "my-eks-cluster"
vpc_id       = "vpc-0abcd1234efgh5678"
# ... other variables
```

3. Initialize and Apply

```sh
terraform init
terraform apply
```

4. Once the cluster is created, configure kubectl to interact with the new EKS cluster by updating your kubeconfig.

```sh
aws eks update-kubeconfig --region [REGION] --name [CLUSTER_NAME]
```

Replace `[REGION]` and `[CLUSTER_NAME]` with the appropriate AWS region and EKS cluster name.

5. Deploy your Kubernetes workloads to the EKS cluster using kubectl.

```sh
kubectl apply -f [K8S_MANIFEST_FILE]
```
Replace `[K8S_MANIFEST_FILE]` with the path to your Kubernetes manifest file.

### Variables available for configuring the Cluster via this module

| Variable Name | Type | Default | Description |
|---------------|------|---------|-------------|
| `cluster_name` | `string` | N/A | **Cluster Name**: A critical variable that uniquely identifies your EKS cluster within AWS. It is used in various AWS API calls and Kubernetes configurations. |
| `vpc_id` | `string` | N/A | **VPC ID**: Crucial for defining the network where your EKS cluster will reside. Ensure that the specified VPC is properly configured for EKS, including necessary security groups, route tables, and internet connectivity. |
| `control_plane_subnet_ids` | `set(string)` | `[]` | **Control Plane Subnet IDs**: Allows you to specify the subnets for your EKS control plane by providing subnet IDs directly. Ensure that the subnets can communicate with the EKS endpoints. |
| `control_plane_subnet_name_selectors` | `set(string)` | `[]` | **Control Plane Subnet Name Selectors**: Allows you to specify the subnets for your EKS control plane by selecting them based on name tags. |
| `node_group_subnet_ids` | `set(string)` | `[]` | **Node Group Subnet IDs**: Define where the EKS worker nodes (node groups) will be deployed by providing subnet IDs directly. |
| `node_group_subnet_name_selectors` | `set(string)` | `[]` | **Node Group Subnet Name Selectors**: Define where the EKS worker nodes (node groups) will be deployed by selecting them based on name tags. |
| `whitelist_public_cidrs` | `set(string)` | `[]` | **Whitelist Public CIDRs**: Controls access to your EKS cluster by specifying which CIDR blocks are allowed to access it. Be mindful of security considerations when whitelisting CIDRs for public access. |
| `whitelist_caller_public_ip` | `bool` | `true` | **Whitelist Caller Public IP**: A flag that determines whether to automatically whitelist the IP address of the caller (you) when applying the Terraform configuration. |
| `enable_serviceroles` | `bool` | `true` | **Enable Service Roles**: Enabling IAM roles for service accounts allows you to assign AWS IAM roles to Kubernetes service accounts, providing a secure mechanism to grant AWS permissions to pods. |
| `logging_config` | `object` | `{api = true, audit = true, authenticator = true, controllerManager = false, scheduler = false}` | **Logging Config**: An object that defines which EKS control plane logging components should be enabled, enhancing observability and troubleshooting capabilities. |
| `log_group_retention_period` | `number` | `7` | **Log Group Retention Period**: Defines the number of days to retain log events in the CloudWatch log group. Adjust according to your logging and audit requirements. |
| `enable_secret_encryption` | `bool` | `true` | **Enable Secret Encryption**: A flag that determines whether to enable KMS encryption for Kubernetes secrets, providing an additional layer of security for sensitive data. |
| `secret_encryption_kms_key_arn` | `string` | `""` | **Secret Encryption KMS Key ARN**: The ARN of the KMS key used for encrypting Kubernetes secrets. If empty and `enable_secret_encryption` is `true`, a new KMS key will be created. |
| `control_plane_security_groups` | `list(string)` | `[]` | **Control Plane Security Groups**: A list of security group IDs to be associated with the EKS control plane, controlling inbound and outbound traffic to the EKS control plane. |
| `kubernetes_version` | `string` | `"1.27"` | **Kubernetes Version**: The desired Kubernetes version for the EKS cluster, allowing you to use specific Kubernetes features and APIs. |
| `node_groups` | `list(object)` | N/A | **Node Groups**: A list of objects, each specifying a configuration for a node group, allowing you to define the instances that will run your workloads. |
| `secret_encryption_key_deletion_window` | `number` | `7` | **Secret Encryption Key Deletion Window**: The number of days to wait before deleting the KMS key used for secret encryption, after it has been removed from the Terraform configuration, allowing you to manage the lifecycle of the KMS key. |

## Architecture Diagram

The architecture of the EKS cluster module is designed to provide a scalable, secure, and manageable Kubernetes environment on AWS. Below is a conceptual diagram of the architecture, which illustrates the main components and their interactions.

```mermaid
graph TB
    subgraph "Amazon EKS"
        cp[Control Plane]
        ng[Node Groups]
        kms[KMS Key for Secret Encryption]
        iam[IAM Roles]
        sg[Security Groups]
        logs[CloudWatch Logs]
    end
    subgraph "Amazon VPC"
        vpc[VPC]
        subnets[Subnets]
    end
    cp -->|API Server Access| iam
    iam -->|Assume Role| cp
    kms -->|Encrypt/Decrypt| cp
    cp -->|Audit & API logs| logs
    ng -->|Worker Nodes| cp
    cp -->|Network Traffic| vpc
    ng -->|Network Traffic| vpc
    vpc -->|Hosts| subnets
    sg -->|Control Traffic| vpc
```

### Explanation
- **Control Plane**: Manages the state of the EKS cluster, ensuring that the desired and actual states of the cluster are in sync.
- **Node Groups**: Host the pods that run your workloads, scaling according to demand and ensuring high availability.
- **VPC & Subnets**: Provide network isolation, ensuring that traffic can be securely routed between your EKS cluster and other resources.
- **IAM Roles**: Enable secure access to AWS services and resources, both from within the EKS cluster and from external entities.
- **KMS Key**: Ensures that Kubernetes secrets are encrypted at rest, safeguarding sensitive data.
Security Groups: Control inbound and outbound traffic to resources, ensuring that only legitimate traffic is allowed.
- **Logging**: Provides insights into the operation of your EKS control plane, aiding in monitoring and troubleshooting.