## Table of Contents

- [Overview](#overview)
- [Understanding `main.tf` Configuration](#understanding-maintf-configuration)
  - [1. Data Blocks](#1-data-blocks)
  - [2. Local Values](#2-local-values)
  - [3. Import Block](#3-import-block)
  - [4. Resource Block](#4-resource-block)
- [Usage](#usage)
  - [Variables available for configuring the Cluster Auth via this module](#variables-available-for-configuring-the-cluster-auth-via-this-module)
  - [Example Usage](#example-usage)

## Overview

This Terraform module manages the `aws-auth` ConfigMap within an Amazon Elastic Kubernetes Service (EKS) cluster, allowing you to control which AWS IAM entities are allowed to interact with your EKS cluster. The module allows you to specify additional IAM roles and users that should be granted access to the EKS cluster, defining how they should be mapped to Kubernetes user entities and groups.

### Understanding `main.tf` Configuration

1. **Data Blocks**

```hcl
data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}

data "kubernetes_config_map" "aws_auth_cm" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }
}
```

- **`data "aws_eks_cluster" "cluster"`**: This data block fetches the details of an existing EKS cluster using the provided cluster_name variable.

- **`data "kubernetes_config_map" "aws_auth_cm"`**: This data block retrieves the existing `aws-auth` ConfigMap from the kube-system namespace of your EKS cluster. This ConfigMap is crucial for controlling access to the EKS cluster by mapping AWS IAM entities to Kubernetes RBAC groups.

2. **Local Values**

```hcl
locals {
  base_map_roles = yamldecode(try(data.kubernetes_config_map.aws_auth_cm.data["mapRoles"], "---"))
  base_map_users = yamldecode(try(data.kubernetes_config_map.aws_auth_cm.data["mapUsers"], "---"))

  combined_map_roles = concat(local.base_map_roles, var.auth_config_map_roles)
  combined_map_users = concat(local.base_map_users, var.auth_config_map_users)
}
```

- **`base_map_roles` and `base_map_users`**: These local values attempt to decode the YAML data from the existing `aws-auth` ConfigMap. If the keys `mapRoles` and `mapUsers` do not exist or are empty, it defaults to an empty YAML document (indicated by "---").

- **`combined_map_roles` and `combined_map_users`**: These local values concatenate the base map roles/users with additional roles/users provided via the `auth_config_map_roles` and `auth_config_map_users` variables. This allows you to add new IAM roles and users without overwriting existing ones.

3. **Import Block**

```hcl
import {
  to = kubernetes_config_map.aws_auth_cm
  id = "kube-system/aws-auth"
}
```

- **`to = kubernetes_config_map.aws_auth_cm`**: This line is setting the destination for the import. `kubernetes_config_map.aws_auth_cm` is a resource address, which refers to the resource that Terraform will manage.

- **`id = "kube-system/aws-auth"`**: This line is specifying the ID of the resource to import. In this case, it’s `kube-system/aws-auth`.

- After running this import command, Terraform will manage the `aws_auth_cm` ConfigMap in the `kube-system` namespace of your Kubernetes cluster. If you make changes to this ConfigMap in your Terraform configuration, those changes will be applied to the actual ConfigMap in your cluster when you run `terraform apply`

4. **Resource Block**

```hcl
resource "kubernetes_config_map" "aws_auth_cm" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }

  data = {
    mapRoles  = yamlencode(local.combined_mapRoles)
    mapUsers = yamlencode(local.mapUsers)
  }
}
```

- **`resource "kubernetes_config_map" "aws_auth_cm"`**: This resource block manages the `aws-auth` ConfigMap in the EKS cluster. It uses the `combined_map_roles` and `combined_map_users` local values (encoded to YAML) to define the `mapRoles` and `mapUsers` data, respectively, of the ConfigMap.

- The import is done prior to this so that Terraform doesn't attempt to create a new resource and run into an error, instead we import the existing one and update it with the config we require.

## Usage

### Variables available for configuring the Cluster Auth via this module

| Variable Name | Type | Description |
|---------------|------|-------------|
| `auth_config_map_roles` | `list(object)` | A list of IAM roles that should be mapped to Kubernetes RBAC groups. Each object within the list specifies the IAM role ARN, the Kubernetes RBAC groups that the role should be mapped to, and optionally, a username that will represent the IAM entity in Kubernetes RBAC policies. <br><br>**Object Attributes:**<br> - `rolearn`: The ARN of the IAM role to map.<br> - `groups`: A set of Kubernetes RBAC groups that the IAM role will be mapped to.<br> - `username`: (Optional) A string representing the IAM entity in Kubernetes RBAC policies. Defaults to `"{{SessionName}}"` if not provided. |
| `auth_config_map_users` | `list(object)` | A list of IAM users that should be mapped to Kubernetes RBAC groups. Each object within the list specifies the IAM user ARN, the Kubernetes RBAC groups that the user should be mapped to, and optionally, a username that will represent the IAM entity in Kubernetes RBAC policies. <br><br>**Object Attributes:**<br> - `userarn`: The ARN of the IAM user to map.<br> - `groups`: A set of Kubernetes RBAC groups that the IAM user will be mapped to.<br> - `username`: (Optional) A string representing the IAM entity in Kubernetes RBAC policies. Defaults to `"{{SessionName}}"` if not provided. |
| `cluster_name` | `string` | The name of the Amazon EKS cluster that the `aws-auth` ConfigMap should be managed in. This variable is utilized to retrieve details of the EKS cluster and to ensure that the IAM role and user mappings are applied to the correct Kubernetes cluster. |

### Example Usage

```hcl
module "eks_cluster_auth" {
  source = "path_to_your_module/cluster_auth"

  cluster_name = "my-eks-cluster"
  auth_config_map_roles = [
    {
      rolearn  = "arn:aws:iam::123456789012:role/EksNodeRole"
      groups   = ["system:bootstrappers", "system:nodes"]
      username = "system:node:{{EC2PrivateDNSName}}"
    }
  ]
  auth_config_map_users = [
    {
      userarn = "arn:aws:iam::123456789012:user/eks-admin"
      groups  = ["system:masters"]
    }
  ]
}
```
