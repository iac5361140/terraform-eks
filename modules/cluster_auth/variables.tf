variable "auth_config_map_roles" {
  type = list(object({
    groups   = set(list)
    rolearn  = string
    username = optional(string, "{{SessionName}}")
  }))
  description = <<EOF
  List of additional AWS IAM roles to be added to the `aws-auth` ConfigMap, allowing the specified IAM roles to interact with the EKS cluster. Each object within the list should contain:
  - `groups`: A set of Kubernetes groups the IAM role should be mapped to.
  - `rolearn`: The Amazon Resource Name (ARN) of the IAM role to be added.
  - `username` (optional): The Kubernetes username to be mapped to the IAM role. Defaults to "{{SessionName}}" if not provided.
  EOF
}

variable "auth_config_map_users" {
  type = list(object({
    groups   = set(list)
    rolearn  = string
    username = optional(string, "{{SessionName}}")
  }))
  description = <<EOF
  List of additional AWS IAM users to be added to the `aws-auth` ConfigMap, allowing the specified IAM users to interact with the EKS cluster. Each object within the list should contain:
  - `groups`: A set of Kubernetes groups the IAM user should be mapped to.
  - `rolearn`: The Amazon Resource Name (ARN) of the IAM user to be added.
  - `username` (optional): The Kubernetes username to be mapped to the IAM user. Defaults to "{{SessionName}}" if not provided.
  EOF
}

variable "cluster_name" {
  type = string
  description = <<EOF
  The name of the Amazon EKS cluster. This name is used to pull the existing EKS cluster details and to configure the `aws-auth` ConfigMap in the Kubernetes `kube-system` namespace, which controls access to the EKS cluster.
  EOF
}
